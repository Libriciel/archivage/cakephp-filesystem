<?php
/**
 * Libriciel\Filesystem\Command\FileSystemCommand
 */

namespace Libriciel\Filesystem\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Assure le rollback en cas d'interuption brutale d'un process du FileSystemUtility
 *
 * A utiliser avec cron
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FilesystemCommand extends Command
{

    /**
     * Vérifi l'existance des processes utilisant le rollback fichier
     * Appel FileSystemCommand::rollback($dirname) si le process n'existe plus
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io The console io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        foreach (glob(Filesystem::TRANSACTION_DIR . DS . '*') as $dirname) {
            $pid = (int)basename($dirname);
            if (!file_exists('/proc/'.$pid)) {
                $io->out(__d('cakephp-filesystem', "Rollback process {0}", $pid));
                Filesystem::rollbackProcess($pid);
            } else {
                $io->out(__d('cakephp-filesystem', '{0} is still running...', $pid));
            }
        }
    }
}
