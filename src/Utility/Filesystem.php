<?php
/**
 * Libriciel\Filesystem\Utility\Filesystem
 */

namespace Libriciel\Filesystem\Utility;

use Cake\Core\Configure;
use Cake\Log\Log;
use Exception;
use DirectoryIterator;
use Symfony\Component\Filesystem\Filesystem as SymFilesystem;
use Symfony\Component\Filesystem\Exception\IOException;
use Traversable;

/**
 * Utilitaire permettan de manipuler les fichiers / dossiers du système
 * Permet d'effectuer des transactions
 *
 * @category    Utility
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Filesystem
{
    const DS = DIRECTORY_SEPARATOR;

    /**
     * @var Filesystem[] instances de la classe
     */
    private static $instances = [];

    /**
     * @var Filesystem Dernière instance utilisé
     */
    private static $activeInstance;

    /**
     * Instance du filesystem Symfony
     *
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private static $SymFilesystem;

    /**
     * @var bool utilisation du shell shred ?
     *           surcharge possible via Configure::read('FilesystemUtility.useShred')
     */
    public static $useShred = false;

    /**
     * @var boolean vrai si l'instance est en cours de transaction
     */
    public $transaction = false;

    /**
     * @var array liste de callbacks à appeler lors du rollback
     */
    public $toCallForRollback = [];

    /**
     * @var array liste de callbacks à appeler lors du commit
     */
    public $toCallForCommit = [];

    /**
     * @var string dossier temporaire utilisé (pour la transaction)
     */
    public $tmpDir;

    /**
     * @see Filesystem::setRemoveMethod()
     * @see Filesystem::useShred()
     * @var callable méthode à utiliser pour la suppression
     */
    public $removeMethod;

    /**
     * Privilégie la propreté du répertoire modifié au détriment des performances
     * Passer à vrai par exemple si vous devez supprimer/déplacer des fichiers
     * d'un répertoire avant de le transformer en zip...
     * @var bool
     */
    public $safeMode = false;

    /**
     * @var string Namespace de l'instance
     */
    private $namespace;

    /**
     * Emplacement pour les transactions format fichier
     */
    const TRANSACTION_DIR = TMP . 'Filesystem';

    /**
     * Constructeur de class multiton
     *
     * @param string $namespace
     * @throws Exception
     */
    private function __construct(string $namespace)
    {
        $namespace = urlencode($namespace);

        if (class_exists('\Cake\Core\Configure')) {
            $pathToData = Configure::read('App.paths.data');
            $tmpdir = Configure::read(
                'FilesystemUtility.tmpDir',
                $pathToData ? $pathToData . self::DS . 'tmp' : sys_get_temp_dir()
            );
        } else {
            $tmpdir = sys_get_temp_dir();
        }
        do {
            $this->tmpDir = $tmpdir . self::DS . uniqid('Filesystem_'.$namespace, true);
        } while (is_dir($this->tmpDir));

        mkdir($this->tmpDir, 0777, true);
        if (!is_dir(self::TRANSACTION_DIR)) {
            mkdir(self::TRANSACTION_DIR, 0777, true);
        }
        if (!is_dir(self::TRANSACTION_DIR)) {
            throw new Exception(self::TRANSACTION_DIR.' is not writable');
        }
        if (!is_dir($this->tmpDir)) {
            throw new Exception($this->tmpDir.' is not writable');
        }
        if (empty(static::$SymFilesystem)) {
            static::$SymFilesystem = new SymFilesystem;
        }
        $this->configure();
        $this->namespace = $namespace;
    }

    /**
     * Configuration de la classe
     *
     * - boolean useShred : utilisera la méthode shred pour éffacer les fichiers (plus long)
     */
    private function configure()
    {
        $config = [];
        if (class_exists('\Cake\Core\Configure')) {
            $config += [
                'useShred' => (boolean)Configure::read('FilesystemUtility.useShred'),
            ];
        }
        $config += ['useShred' => self::$useShred];
        $this->removeMethod = $config['useShred']
            ? [$this, 'shred']
            : [static::$SymFilesystem, 'remove'];
    }

    /**
     * Donne la dernière instance
     *
     * @param string $namespace
     * @return Filesystem
     * @throws Exception
     */
    public static function getActiveInstance(string $namespace = 'default'): Filesystem
    {
        if (empty(static::$activeInstance)) {
            return static::getInstance($namespace);
        }
        return static::$activeInstance;
    }

    /**
     * Permet de passer d'un namespace à l'autre
     * Permet également d'initialiser l'utilitaire pour une utilisation sans transaction
     *
     * @param string $namespace
     * @return Filesystem
     * @throws Exception
     */
    public static function setNamespace(string $namespace = 'default'): Filesystem
    {
        return static::getInstance($namespace);
    }

    /**
     * Permet de passer d'un namespace à l'autre
     * Permet également d'initialiser l'utilitaire pour une utilisation sans transaction
     *
     * @param callable $callback
     * @return Filesystem
     * @throws Exception
     */
    public static function setRemoveMethod(callable $callback): Filesystem
    {
        $instance = static::getActiveInstance();
        $instance->removeMethod = $callback;
        return $instance;
    }

    /**
     * Permet d'utiliser ou pas la méthode shred
     *
     * @param boolean $use
     * @return Filesystem
     * @throws Exception
     */
    public static function useShred(bool $use = true): Filesystem
    {
        $callable = $use
            ? [static::getActiveInstance(), 'shred']
            : [static::$SymFilesystem, 'remove'];
        return static::setRemoveMethod($callable);
    }

    /**
     * L'activation du safeMode permet de garder un répertoire "propre" pendant
     * la durée de la transaction.
     *
     * /!\ L'activation du safeMode à un effet négatif sur les performances
     *
     * Ex: On veut supprimer un fichier avant de déplacer le répertoire qui le
     * contient. Si le safeMode n'est pas activé, le repertoire va contenir la
     * sauvegarde du fichier. Si le safeMode est activé, la sauvegarde se situera
     * dans le dossier temporaire du système.
     *
     * @param boolean $safeMode
     * @throws Exception
     */
    public static function setSafemode(bool $safeMode)
    {
        static::getActiveInstance()->safeMode = $safeMode;
    }

    /**
     * Pertmet d'obtenir l'instance selon un namespace
     *
     * @param string $namespace = 'default'
     * @return Filesystem
     * @throws Exception
     */
    public static function getInstance(string $namespace = 'default'): Filesystem
    {
        if (empty(static::$instances[$namespace])) {
            static::$instances[$namespace] = new Filesystem($namespace);
        }
        static::$activeInstance = static::$instances[$namespace];
        return static::$activeInstance;
    }

    /**
     * Démarre la transaction pour un namespace donné
     *
     * @param string $namespace
     * @return Filesystem
     * @throws Exception
     */
    public static function begin(string $namespace = 'default'): Filesystem
    {
        $instance = static::getInstance($namespace);
        $instance->transaction = true;
        return $instance;
    }

    /**
     * Défait les actions pour un namespace donné et termine la transaction
     *
     * @param string $namespace
     * @return Filesystem
     * @throws Exception
     */
    public static function rollback(string $namespace = 'default'): Filesystem
    {
        $instance = static::getInstance($namespace);
        $instance->rollbackInstance();
        return $instance;
    }

    /**
     * Termine la transaction pour un namespace donné
     *
     * @param string $namespace
     * @return Filesystem
     * @throws Exception
     */
    public static function commit(string $namespace = 'default'): Filesystem
    {
        $instance = static::getInstance($namespace);
        $actions = array_values($instance->toCallForCommit);
        for ($i = count($actions) -1; $i >= 0; $i--) {
            call_user_func_array($actions[$i]['callback'], $actions[$i]['args']);
        }
        $instance->resetTransaction();

        return $instance;
    }

    /**
     * Permet d'obtenir un chemin pour le process
     *
     * @param int $pid
     * @return string
     */
    private static function getProcessPath(int $pid = 0): string
    {
        if ($pid === 0) {
            $pid = getmypid();
        }
        return self::TRANSACTION_DIR . self::DS . $pid;
    }

    /**
     * Permet d'obtenir un chemin pour stocker les valeurs de rollback sous
     * format fichier
     *
     * @return string
     */
    private function getRollbackPath(): string
    {
        return static::getProcessPath() . self::DS . $this->namespace;
    }

    /**
     * Donne le chemin du fichier de sauvegarde du rollback
     *
     * @return string
     */
    private function getRollbackFilename(): string
    {
        return $this->getRollbackPath() . self::DS . count($this->toCallForRollback) . '.json';
    }

    /**
     * Remet à zéro la to-do liste du rollback
     */
    private function resetTransaction()
    {
        $this->transaction = false;
        $this->toCallForRollback = [];
        $this->toCallForCommit = [];
        static::callSymfony('remove', $this->getRollbackPath());
        if (empty(glob(static::getProcessPath() . self::DS . '*'))) {
            static::callSymfony('remove', static::getProcessPath());
        }
    }

    /**
     * Actions de rollback pour une instance donnée
     *
     * @param bool $safemode Si vrai, envoi une erreur plutôt qu'une exception
     * @throws Exception
     */
    public function rollbackInstance(bool $safemode = false)
    {
        $actions = array_values($this->toCallForRollback);
        for ($i = count($actions) -1; $i >= 0; $i--) {
            try {
                call_user_func_array($actions[$i]['callback'], $actions[$i]['args']);
            } catch (Exception $e) {
                if ($safemode) {
                    trigger_error($e->getMessage());
                } else {
                    throw $e;
                }
            }
        }
        $this->resetTransaction();
    }

    /**
     * Copies a file.
     *
     * If the target file is older than the origin file, it's always overwritten.
     * If the target file is newer, it is overwritten only when the
     * $overwriteNewerFiles option is set to true.
     *
     * @param string $originFile          The original filename
     * @param string $targetFile          The target filename
     * @param bool   $overwriteNewerFiles If true, target files newer than origin files are overwritten
     * @throws Exception
     */
    public static function copy(string $originFile, string $targetFile, bool $overwriteNewerFiles = false)
    {
        if (!is_file($originFile)) {
            $msg = function_exists('__d')
                ? __d(
                    'cakephp-filesystem',
                    "Failed to copy because the origin \"{0}\" does not exists.",
                    $targetFile
                )
                : sprintf('Failed to copy because the origin "%s" does not exists.', $targetFile);
            throw new IOException($msg, 0, null, $targetFile);
        }
        $instance = static::getActiveInstance();
        $dirname = dirname($targetFile);
        if (!is_dir($dirname)) {
            static::mkdir($dirname);
        }
        static::writableCheck($dirname);
        if ($overwriteNewerFiles && is_file($targetFile)) {
            static::writableCheck($targetFile);
        }

        $doCopy = true;
        if (!$overwriteNewerFiles && null === parse_url($originFile, PHP_URL_HOST) && is_file($targetFile)) {
            $doCopy = filemtime($originFile) > filemtime($targetFile);
        }

        if ($instance->transaction && $doCopy) {
            if (is_file($targetFile)) {
                $instance->saveFile($targetFile);
            } else {
                $instance->deleteIfRollback($targetFile);
            }
        }

        static::callSymfony('copy', $originFile, $targetFile, $overwriteNewerFiles);
    }

    /**
     * Removes files or directories.
     *
     * @param string|array|Traversable $files A filename, an array of files, or a \Traversable instance to remove
     *
     * @throws IOException When removal fails
     * @throws Exception
     */
    public static function remove($files)
    {
        $instance = static::getActiveInstance();
        if ($files instanceof Traversable) {
            $files = iterator_to_array($files, false);
        } elseif (!is_array($files)) {
            $files = array($files);
        }
        $files = array_reverse($files);
        $safeMode = static::getActiveInstance()->safeMode;
        foreach ($files as $file) {
            static::writableCheck($file);
            if (is_link($file)) {
                // See https://bugs.php.net/52176
                if (!@(unlink($file) || '\\' !== DIRECTORY_SEPARATOR || rmdir($file)) && file_exists($file)) {
                    $error = error_get_last();
                    $msg = function_exists('__d')
                        ? __d(
                            'cakephp-filesystem',
                            'Failed to remove symlink "{0}": {1}.',
                            $file,
                            $error['message']
                        )
                        : sprintf("Failed to remove symlink \"%s\": %s.", $file, $error['message']);
                    throw new IOException($msg);
                }
            } elseif (is_dir($file)) {
                // déplace les fichiers dans un dossier temporaire
                static::getActiveInstance()->setSafemode(true);
                if ($instance->transaction) {
                    $instance->saveFile($file);
                }
                call_user_func($instance->removeMethod, $file);

                if (is_dir($file) && !@rmdir($file) && file_exists($file)) {
                    $error = error_get_last();
                    $msg = function_exists('__d')
                        ? __d(
                            'cakephp-filesystem',
                            'Failed to remove directory "{0}": {1}.',
                            $file,
                            $error['message']
                        )
                        : sprintf("Failed to remove directory \"%s\": %s.", $file, $error['message']);
                    throw new IOException($msg);
                }
            } else {
                if ($instance->transaction) {
                    $instance->saveFile($file);
                }
                call_user_func($instance->removeMethod, $file);
            }
        }
        static::getActiveInstance()->setSafemode($safeMode);
    }

    /**
     * Creates a directory recursively.
     *
     * @param string|array|Traversable $dirs The directory path
     * @param int                      $mode The directory mode
     * @throws Exception
     */
    public static function mkdir($dirs, int $mode = 0777)
    {
        $instance = static::getActiveInstance();
        if (!$instance->transaction) {
            static::callSymfony('mkdir', $dirs, $mode);
            return;
        }
        foreach ((array)$dirs as $dir) {
            $path = '';
            foreach (explode(self::DS, trim($dir, self::DS)) as $dirName) {
                $path .= self::DS . $dirName;
                if (!is_dir($path)) {
                    static::writableCheck(dirname($path));
                    $instance->deleteIfRollback($path);
                    static::callSymfony('mkdir', $path, $mode);
                }
            }
        }
    }

    /**
     * Renames a file or a directory.
     *
     * @param string $origin    The origin filename or directory
     * @param string $target    The new filename or directory
     * @param bool   $overwrite Whether to overwrite the target if it already exists
     * @throws Exception
     */
    public static function rename(string $origin, string $target, bool $overwrite = false)
    {
        if (!file_exists($origin)) {
            $msg = function_exists('__d')
                ? __d(
                    'cakephp-filesystem',
                    "Cannot rename because the origin \"{0}\" does not exists.",
                    $target
                )
                : sprintf('Cannot rename because the origin "%s" does not exists.', $target);
            throw new IOException($msg, 0, null, $target);
        }
        $instance = static::getActiveInstance();
        $dirname = dirname($target);
        if (!is_dir($dirname)) {
            static::mkdir($dirname);
        }
        static::writableCheck($dirname);
        if ($overwrite && $instance->transaction && is_file($target)) {
            static::writableCheck($target);
            $instance->tmpRename($target);
        }
        if ($instance->transaction) {
            $instance->renameIfRollback($target, $origin);
        }
        static::callSymfony('rename', $origin, $target, $overwrite);
    }

    /**
     * Renomme le temps de la transaction un fichier (il reste dans le même répertoire)
     * @param string $filename
     */
    private function tmpRename(string $filename)
    {
        $tmpName = static::findTmpName($filename);
        static::writableCheck(dirname($tmpName));
        $this->renameIfRollback($tmpName, $filename);
        static::callSymfony('rename', $filename, $tmpName, false);
        $this->deleteIfCommit($tmpName);
    }

    /**
     * Donne un nom temporaire unique pour un fichier
     *
     * ex: /tmp/test.txt > /tmp/.test.txt_1.bak
     * @param string $filename
     * @return string
     */
    private static function findTmpName(string $filename): string
    {
        $base = dirname($filename).self::DS.'.'.basename($filename).'_';
        $i = 0;
        while (file_exists($base . $i . '.bak')) {
            $i++;
        }
        return $base . $i . '.bak';
    }

    /**
     * Atomically dumps content into a file.
     *
     * @param string $filename The file to be written to
     * @param mixed  $content  The data to write into the file
     * @throws IOException
     * @throws Exception
     */
    public static function dumpFile(string $filename, $content)
    {
        $instance = static::getActiveInstance();
        $dirname = dirname($filename);
        if (!is_dir($dirname)) {
            static::mkdir($dirname);
        }
        static::writableCheck(dirname($filename));
        if ($instance->transaction) {
            if (is_file($filename)) {
                static::writableCheck($filename);
                $instance->saveFile($filename);
            } else {
                $instance->deleteIfRollback($filename);
            }
        }

        static::callSymfony('dumpFile', $filename, $content);
    }

    /**
     * Copie le fichier dans le dossier temporaire avec son chemin complet
     * ex: /data/Dossier1/file1.pdf => /tmp/Filesystem_default5915b375934a6/0/data/Dossier1/file1.pdf
     * Sera remis sur son emplacement en cas de rollback
     * (écrase le fichier en place si il existe)
     *
     * @param string $filename
     */
    private function saveFile(string $filename)
    {
        $index = count($this->toCallForRollback);
        $targetFilename = $this->safeMode ?
            $this->tmpDir . self::DS . $index . $filename
            : static::findTmpName($filename);

        // On créer le dossier temporaire qui sera supprimé en cas de commit || rollback
        $dir = dirname($targetFilename);
        $newDirs = [];
        if (!is_dir($dir)) {
            $path = '';
            foreach (explode(self::DS, trim($dir, self::DS)) as $dirName) {
                $path .= self::DS . $dirName;
                if (!is_dir($path)) {
                    static::writableCheck(dirname($path));
                    $newDirs[] = $path;
                    static::callSymfony('mkdir', $path);
                    $this->deleteIfRollback($dir);
                }
            }
            $this->deleteIfCommit($newDirs[0]);
        }

        static::callSymfony('rename', $filename, $targetFilename, true);
        $this->renameIfRollback($targetFilename, $filename);

        $this->deleteIfCommit($targetFilename);

        if ($newDirs) {
            $this->deleteIfCommit($newDirs[0]);
            foreach ($newDirs as $dir) {
                $this->deleteIfRollback($dir);
            }
        }
    }

    /**
     * Ajoute une fonction à appeler lors d'un rollback
     *
     * @param callable $callback
     * @param array    $args
     */
    private function toCallForRollback(callable $callback, array $args)
    {
        $file = $this->getRollbackFilename();
        $data = compact('callback', 'args');
        if (!is_dir(dirname($file))) {
            static::callSymfony('mkdir', dirname($file));
        }
        static::callSymfony('dumpFile', $file, json_encode($data));
        $this->toCallForRollback[] = $data;
    }

    /**
     * Ajoute une fonction à appeler lors du commit
     *
     * @param callable $callback
     * @param array    $args
     * @noinspection PhpSameParameterValueInspection $callback = [__CLASS__, 'callRemove'];
     */
    private function toCallForCommit(callable $callback, array $args)
    {
        $data = compact('callback', 'args');
        $this->toCallForCommit[] = $data;
    }

    /**
     * Supprime un/des fichier(s) et/ou un/des répertoire(s) si rollback
     *
     * @param string|array $filename
     */
    private function deleteIfRollback($filename)
    {
        $this->toCallForRollback([__CLASS__, 'callRemove'], [$filename]);
    }

    /**
     * Remet le nom d'origine d'un fichier
     *
     * @param string $filename
     * @param string $afterRollbackFilename
     */
    private function renameIfRollback(string $filename, string $afterRollbackFilename)
    {
        $this->toCallForRollback([__CLASS__, 'callRename'], [$filename, $afterRollbackFilename, true]);
    }

    /**
     * Supprime un/des fichier(s) et/ou un/des répertoire(s) si commit
     *
     * @param string|array $filename
     */
    private function deleteIfCommit($filename)
    {
        $this->toCallForCommit([__CLASS__, 'callRemove'], [$filename]);
    }

    /**
     * Appel le rollback dans la destruction (si transaction en cours)
     */
    public function __destruct()
    {
        try {
            if ($this->transaction) {
                $this->rollbackInstance();
            }
            if ($this->tmpDir && is_dir($this->tmpDir) && !$this->transaction) {
                call_user_func($this->removeMethod, $this->tmpDir);
            }
        } catch (Exception $e) { // Evite de briser les autres destructeurs en cas d'exception
            trigger_error($e->getMessage());
        }
    }

    /**
     * Extraction de l'arborescence selon un array de fichiers[$path]['path']
     *
     * @param array $files
     * @return array
     */
    public static function extractArbo(array $files): array
    {
        $paths = [];
        foreach ($files as $filename) {
            $tmp =& $paths;
            foreach (explode(self::DS, trim($filename, self::DS)) as $dir) {
                $tmp =& $tmp[$dir];
            }
            $tmp = $filename;
            unset($tmp);
        }
        return $paths;
    }

    /**
     * Permet d'obtenir le chemin de fichier en commun entre les fichiers
     *
     * @param array $files
     * @return string chemin en commun entre les fichiers ex : /tmp/dir1
     */
    public static function baseDir(array $files): string
    {
        return static::baseDirExtractor(static::extractArbo($files));
    }

    /**
     * Fonction récursive pour récupérer le chemin en commun d'une liste
     * de fichiers
     * @param array $arbo
     * @param array $keys
     * @return string
     */
    private static function baseDirExtractor(array $arbo, array $keys = []): string
    {
        if (count($arbo) === 1 && is_array(current($arbo))) {
            $key = array_keys($arbo)[0];
            $keys[] = $key;
            return static::baseDirExtractor(current($arbo), $keys);
        }
        return self::DS . implode(self::DS, $keys);
    }

    /**
     * Permet la création d'un fichier vide d'une taille donné
     * NOTE : ne fait pas parti des transactions, n'a d'interet que pour des tests
     *
     * @link http://stackoverflow.com/questions/3608383/php-create-file-with-given-size
     *
     * @param string $filename
     * @param int    $size
     * @throws Exception
     */
    public static function createDummyFile(string $filename, int $size)
    {
        if ($size > 4294967296 && PHP_INT_SIZE < 8) {
            $msg = function_exists('__d')
                ? __d('cakephp-filesystem', "La taille indiqué est trop importante pour votre système")
                : "La taille indiqué est trop importante pour votre système";
            throw new Exception($msg);
        }
        $dirname = dirname($filename);
        if (!is_dir($dirname)) {
            static::mkdir($dirname);
        }

        $f = fopen($filename, 'wb');
        if ($size >= 1000000000) {
            $z = ($size / 1000000000);
            if (is_float($z)) {
                $z = round($z);
                fseek($f, ($size - ($z * 1000000000) -1), SEEK_END);
                fwrite($f, "\0");
            }
            while (--$z > -1) {
                fseek($f, 999999999, SEEK_END);
                fwrite($f, "\0");
            }
        } else {
            fseek($f, $size - 1, SEEK_END);
            fwrite($f, "\0");
        }
        fclose($f);
    }

    /**
     * Rends un fichier impossible à récupérer en y insérant des valeurs aléatoires
     * @param string $filename
     * @param array  $params
     *      - int iterations: nombre de passes
     *      - bool remove: supprime le fichier après l'avoir shreder
     *      - bool zero: remet tous les bits à 0 après le shred
     * @throws Exception
     */
    public static function shred(string $filename, array $params = [])
    {
        if (!file_exists($filename)) {
            return;
        }
        if (is_dir($filename)) {
            foreach (glob($filename.'/{*,.*}', GLOB_BRACE) as $file) {
                if (!in_array(basename($file), ['.', '..'])) {
                    self::shred($file, $params);
                }
            }
            rmdir($filename);
            return;
        }
        $params += [
            'iterations' => 3,
            'remove' => true,
            'zero' => false,
        ];
        $handle = fopen($filename, 'r+');
        $filesize = filesize($filename);
        $toWrite = $filesize;
        $rand = random_bytes(8192);

        while (!feof($handle) && $toWrite > 0) {
            $length = $toWrite > 8192 ? 8192 : $toWrite;
            $toWrite -= $length;
            fwrite($handle, substr($rand, 0, $length));
        }

        $params['iterations']--;
        if ($params['iterations']) {
            fclose($handle);
            self::shred($filename, $params);
        } else {
            if ($params['zero']) {
                rewind($handle);
                $toWrite = $filesize;
                $zero = str_repeat(chr(0), 8192);
                while (!feof($handle) && $toWrite > 0) {
                    $length = $toWrite > 8192 ? 8192 : $toWrite;
                    $toWrite -= $length;
                    fwrite($handle, substr($zero, 0, $length));
                }
            }
            fclose($handle);
            if ($params['remove']) {
                unlink($filename);
            }
        }
    }

    /**
     * Ajoute le/les fichiers/dossier à la liste des choses à supprimer en cas
     * de rollback de la transaction.
     *
     * @param string|array|Traversable $files
     * @throws Exception
     */
    public static function addToDeleteIfRollback($files)
    {
        $instance = static::getActiveInstance();
        if (!$instance->transaction) {
            return;
        }
        if ($files instanceof Traversable) {
            $files = iterator_to_array($files, false);
        } elseif (!is_array($files)) {
            $files = array($files);
        }

        foreach ($files as $file) {
            $instance->deleteIfRollback($file);
        }
    }

    /**
     * Remet le fichier d'origine en cas de rollback
     *
     * @param string|array|Traversable $files
     * @throws Exception
     */
    public static function revertFileIfRollback($files)
    {
        $instance = static::getActiveInstance();
        if (!$instance->transaction) {
            return;
        }
        if ($files instanceof Traversable) {
            $files = iterator_to_array($files, false);
        } elseif (!is_array($files)) {
            $files = array($files);
        }

        foreach ($files as $file) {
            $instance->saveFile($file);
        }
    }

    /**
     * Appel SymFilesystem->remove
     *
     * @return mixed Probablement void
     */
    public static function callRemove()
    {
        $args = ['remove', func_get_args()];
        return call_user_func_array([__CLASS__, 'call'], $args);
    }

    /**
     * Appel SymFilesystem->rename
     *
     * @return mixed Probablement void
     */
    public static function callRename()
    {
        $args = ['rename', func_get_args()];
        return call_user_func_array([__CLASS__, 'call'], $args);
    }

    /**
     * Appel SymFilesystem->$name
     *
     * @param string $name
     * @param array  $args
     */
    private static function call(string $name, array $args)
    {
        static::$SymFilesystem = static::$SymFilesystem ?: new SymFilesystem;
        if (is_callable([static::$SymFilesystem, $name])) {
            static::callSymfony($name, ...$args);
            return;
        }
        $msg = function_exists('__d')
            ? __d('cakephp-filesystem', 'Call to undefined method {0}::{1}()', __CLASS__, $name)
            : sprintf("Call to undefined method %s::%s()", __CLASS__, $name);
        trigger_error($msg);
    }

    /**
     * Effectue un rollback fichier sur le process id actif
     */
    public static function rollbackAll()
    {
        foreach (static::$instances as $instance) {
            if ($instance->transaction) {
                $instance->rollbackInstance(true);
            }
        }
    }

    /**
     * Effectue le rollback d'un process
     * @param int $pid
     */
    public static function rollbackProcess(int $pid)
    {
        static::rollbackDirname(static::getProcessPath($pid));
    }

    /**
     * Effectu le rollback enregistré dans les fichiers
     *
     * @param string $dirname
     */
    private static function rollbackDirname(string $dirname)
    {
        foreach (glob($dirname . self::DS . '*') as $namespace) {
            $files = glob($namespace . self::DS . '*');
            uasort(
                $files,
                function ($a, $b) {
                    $a = (int)pathinfo($a)['filename'];
                    $b = (int)pathinfo($b)['filename'];
                    return ($a > $b) ? -1 : 1;
                }
            );
            static::rollbackFileList($files, $namespace);
        }
        if (empty(glob($dirname . self::DS . '*'))) {
            rmdir($dirname);
        }
    }

    /**
     * Effectue le rollback fichier à partir d'un array de fichiers json
     *
     * @param array  $files
     * @param string $namespace
     */
    private static function rollbackFileList(array $files, string $namespace)
    {
        $error = false;
        foreach ($files as $filename) {
            if (pathinfo($filename)['extension'] !== 'json') {
                unlink($filename);
            } elseif (static::callFilename($filename) === false) {
                $error = true;
                break;
            }
        }
        if ($error === false) {
            Filesystem::callRemove($namespace);
        }
    }

    /**
     * Appel la fonction du fichier json
     *
     * @param string $filename
     * @return bool
     */
    private static function callFilename(string $filename): bool
    {
        try {
            $callback = json_decode(file_get_contents($filename), true);
            call_user_func_array($callback['callback'], $callback['args']);
            unlink($filename);
            return true;
        } catch (Exception $e) {
            if (class_exists('\Cake\Log\Log')) {
                Log::error($e->getMessage());
            }
            unlink($filename);
            return false;
        }
    }

    /**
     * Vérifi qu'un chemin est inscriptible, renvoi une exception sinon
     *
     * Note, is_writable renvoi faux sur le dossier temporaire du système;
     * Il n'y a pas de raisons valable qu'un fichier ne soit pas accéssible par
     * apache dans ce dossier, donc on n'envoi pas d'exception dans ce cas.
     *
     * @param string $filename
     * @throws IOException
     */
    private static function writableCheck(string $filename)
    {
        if (!is_writable($filename)) {
            $msg = function_exists('__d')
                ? __d('cakephp-filesystem', 'Unable to write to the "{0}" filename.', $filename)
                : sprintf("Unable to write to the \"%s\" filename.", $filename);
            throw new IOException($msg, 0, null, $filename);
        }
    }

    /**
     * Permet de remetre à zéro l'utilitaire (drop les transactions)
     * Utile pour les tests unitaires
     */
    public static function reset()
    {
        foreach (static::$instances as $instance) {
            $instance->transaction = false;
            $instance->toCallForRollback = [];
            $instance->toCallForCommit = [];
            $instance->safeMode = false;
        }
    }

    /**
     * Appel Filesystem de Symfony. On s'assure qu'il ne renvoi pas d'exception
     * sur une erreur d'une fonction avec suppresseur
     * @param string $action
     * @param mixed  ...$params
     */
    private static function callSymfony(string $action, ...$params)
    {
        set_error_handler(null);
        forward_static_call_array([static::$SymFilesystem, $action], $params);
        restore_error_handler();
    }

    /**
     * Liste la totalité des fichiers d'un dossier
     * @param string $dir
     * @param bool   $withDirs
     * @return array
     */
    public static function listFiles(string $dir, bool $withDirs = false): array
    {
        if (!is_readable($dir)) {
            return [];
        }
        $iterator = new DirectoryIterator($dir);
        $files = [];
        /** @var DirectoryIterator $item */
        foreach ($iterator as $item) {
            if (!$item->isDot()) {
                if ($item->isDir()) {
                    if ($withDirs) {
                        $files[] = $item->getPathname() . "/";
                    }
                    $files = array_merge(self::listFiles("$dir/$item", $withDirs), $files);
                } else {
                    $files[] = $item->getPathname();
                }
            }
        }
        sort($files);
        return $files;
    }

    /**
     * Supprime les dossiers vide
     * @link https://stackoverflow.com/questions/1833518/remove-empty-subfolders-with-php
     * @param string $path
     * @return bool
     */
    public static function removeEmptySubFolders($path)
    {
        if (!is_dir($path)) {
            return null;
        }
        $empty = true;
        foreach (glob($path.DS."{*,.[!.]*,..?*}", GLOB_BRACE) as $file) {
            $empty = $empty && is_dir($file) && self::removeEmptySubFolders($file);
        }
        return $empty && rmdir($path);
    }

    /**
     * Remet la classe à son état initial
     * @return void
     */
    public static function clear()
    {
        self::$instances = [];
        self::$activeInstance = null;
        self::$SymFilesystem = null;
        self::$useShred = false;
    }
}
