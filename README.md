# Filesystem
[![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-filesystem/badges/master/coverage.svg)](https://asalae2.dev.libriciel.fr/coverage/cakephp-filesystem/index.html)

## Description

Permet de manipuler des fichiers, avec ou sans système de transaction.

## Installation

```bash
composer config repositories.libriciel/cakephp-filesystem git https://gitlab.libriciel.fr/lganee/cakephp-filesystem.git
composer require libriciel/cakephp-filesystem ~1.0
```

Pour bénéficier des fichiers de traduction (Exceptions et Shell), vous pouvez ajouter au chemin de votre application celui du plugin :
```php
Configure::write('App.paths.locales', [
    APP . 'Locale' . DS,
    ROOT.DS.'vendor'.DS.'libriciel'.DS.'cakephp-filesystem'.DS.'src'.DS.'Locale'.DS
]);
```

## Utilisation

En PSR-4, ajoutez un `use Libriciel\Filesystem\Utility\Filesystem;`

### Transactions

```php
Filesystem::begin();
Filesystem::remove($files);
if ($condition) {
    Filesystem::commit();
} else {
    Filesystem::rollback();
}
```

Rollback en cas d'arret du script

```php
Filesystem::begin();
Filesystem::rename($filename, $newFilename);
exit; // rollback
```

Supporte plusieurs niveaux de transaction

```php
Filesystem::begin();
Filesystem::rename($tmpFile1, $tmpFile2, true);

    Filesystem::begin('nested');
    Filesystem::rename($tmpFile2, $tmpFile3, true);
    if ($cond) {
        Filesystem::rollback('nested');
    }

if ($cond) {
    Filesystem::rollback();
}
```

### Shell

Un shell existe afin d'effectuer les rollbacks qui n'ont pas pu être effectués (ex: arrêt brutal du serveur).
À utiliser avec le binaire de cakephp
```bash
bin/cake \\Libriciel\\Filesystem\\Shell\\FilesystemShell
```

### Shred

Sur système Unix, le programme shred peut être utilisé pour supprimer bit par bit un fichier.
Pour l'utiliser avec Filesystem, vous devez passer la clef de configuration `Configure::write('FilesystemUtility.useShred', true)`.
Vous pouvez changer à tout moment cette valeur en appelant :
```php
Filesystem::useShred($bool);
```

## Tests

```bash
vendor/bin/phpunit
```