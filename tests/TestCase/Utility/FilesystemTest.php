<?php

namespace Libriciel\Filesystem\Test\TestCase\Utility;

use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Exception\IOException;

class FilesystemTest extends TestCase
{
    public $tmpFile1 = '';
    public $tmpFile2 = '';

    public function setUp(): void
    {
        Filesystem::reset();
        $garbages = glob(sys_get_temp_dir() . DS . 'testunit*');
        if (!empty($garbages)) {
            Filesystem::setNamespace('no-transaction');
            Filesystem::remove($garbages);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    public function testCopyCreate()
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'testunit-');
        $content = "Fichier crée pour un test unitaire";
        file_put_contents($tmpFile, $content);
        $destination = sys_get_temp_dir() . DS . 'testunit-destination' . DS . uniqid('gen');

        Filesystem::begin();
        Filesystem::copy($tmpFile, $destination);

        $this->assertTrue(is_file($destination), 'Copie du fichier');

        Filesystem::rollback();

        $this->assertFalse(is_file($destination), 'Rollback');

        Filesystem::begin();
        Filesystem::copy($tmpFile, $destination);
        Filesystem::commit();
        $this->assertTrue(is_file($destination), 'Commit');
        $this->assertEquals($content, file_get_contents($destination), 'Vérification du contenu');

        $this->safeUnlink($tmpFile);
        $this->safeUnlink($destination);
    }

    public function testCopyOverride()
    {
        $tmpFile2 = tempnam(sys_get_temp_dir(), 'testunit-');
        sleep(1); // assure une différence au timestamp des deux fichiers
        $tmpFile1 = tempnam(sys_get_temp_dir(), 'testunit-');
        $content1 = "Fichier crée pour un test unitaire";
        $content2 = "Fichier crée pour un test unitaire - fichier modifié";
        file_put_contents($tmpFile1, $content1);
        file_put_contents($tmpFile2, $content2);

        Filesystem::begin();
        // Note: le fichier de $tmpFile1 est plus récent (ç'est important)
        Filesystem::copy($tmpFile1, $tmpFile2);

        $this->assertNotEquals($content1, file_get_contents($tmpFile2), 'La copie ne doit pas avoir eu lieu car fichier existant');

        Filesystem::rollback();

        $this->assertEquals($content1, file_get_contents($tmpFile1), 'Rien ne doit avoir changé 1');
        $this->assertEquals($content2, file_get_contents($tmpFile2), 'Rien ne doit avoir changé 2');

        Filesystem::begin();
        Filesystem::copy($tmpFile1, $tmpFile2, true);

        $this->assertEquals($content1, file_get_contents($tmpFile2), 'La copie doit avoir eu lieu');

        Filesystem::rollback();

        $this->assertEquals($content1, file_get_contents($tmpFile1), 'Rien ne doit avoir changé 3');
        $this->assertEquals($content2, file_get_contents($tmpFile2), 'Rien ne doit avoir changé 4');

        Filesystem::begin();
        Filesystem::copy($tmpFile1, $tmpFile2, true);

        $this->assertEquals($content1, file_get_contents($tmpFile2), 'Doit avoir changé');
        Filesystem::commit();
        $this->assertEquals($content1, file_get_contents($tmpFile2), 'Doit avoir changé après commit');

        $this->safeUnlink($tmpFile1);
        $this->safeUnlink($tmpFile2);
    }

    public function testMkdir()
    {
        $basePath = sys_get_temp_dir() . DS . 'testunit-mkdir';
        $dirToMake = $basePath . DS . 'with' . DS . 'Some' . DS . 'deep';

        Filesystem::begin();
        Filesystem::mkdir($dirToMake);
        Filesystem::rollback();
        $this->assertFalse(is_dir($dirToMake), 'Rollback mkdir');

        Filesystem::begin();
        Filesystem::mkdir($dirToMake);
        Filesystem::commit();
        $this->assertTrue(is_dir($dirToMake), 'Commit mkdir');

        exec('rm '.escapeshellarg($basePath).' -R');
    }

    public function testRename()
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'testunit-');
        $content = "Fichier crée pour un test unitaire";
        file_put_contents($tmpFile, $content);
        $destination = sys_get_temp_dir() . DS . 'testunit-destination' . DS . uniqid('gen');

        Filesystem::begin();
        Filesystem::rename($tmpFile, $destination);

        $this->assertTrue(is_file($destination), 'Rename du fichier');
        $this->assertFalse(is_file($tmpFile), "N'est pas une copie");

        Filesystem::rollback();

        $this->assertFalse(is_file($destination), 'Rollback du fichier 1');
        $this->assertTrue(is_file($tmpFile), "Rollback du fichier 2");

        Filesystem::begin();
        Filesystem::rename($tmpFile, $destination);
        Filesystem::commit();

        $this->assertTrue(is_file($destination), 'Rename du fichier - commité');
        $this->assertFalse(is_file($tmpFile), "N'est pas une copie - commité");

        $this->safeUnlink($tmpFile);
        $this->safeUnlink($destination);
    }

    public function testRenameException()
    {
        $this->expectException(IOException::class);
        $this->tmpFile1 = tempnam(sys_get_temp_dir(), 'testunit-');
        $this->tmpFile2 = tempnam(sys_get_temp_dir(), 'testunit-');
        Filesystem::setNamespace('no-transaction');
        Filesystem::rename($this->tmpFile1, $this->tmpFile2, false);
    }

    public function testRecoverFromPrevException()
    {
        $this->safeUnlink($this->tmpFile1);
        $this->safeUnlink($this->tmpFile2);
        $this->assertFalse(is_file($this->tmpFile1));
        $this->assertFalse(is_file($this->tmpFile2));
    }

    public function testRenameOverride()
    {
        $tmpFile1 = tempnam(sys_get_temp_dir(), 'testunit-');
        $tmpFile2 = tempnam(sys_get_temp_dir(), 'testunit-');
        $content1 = "Fichier crée pour un test unitaire";
        $content2 = "Fichier crée pour un test unitaire - fichier modifié";
        file_put_contents($tmpFile1, $content1);
        file_put_contents($tmpFile2, $content2);

        Filesystem::setNamespace('no-transaction');
        Filesystem::rename($tmpFile1, $tmpFile2, true);

        $this->assertFalse(is_file($tmpFile1), 'Le fichier a été renommé');
        $this->assertEquals($content1, file_get_contents($tmpFile2), "Il a écraser l'autre fichier");

        $this->safeUnlink($tmpFile1);
        $this->safeUnlink($tmpFile2);

        $tmpFile1 = tempnam(sys_get_temp_dir(), 'testunit-');
        $tmpFile2 = tempnam(sys_get_temp_dir(), 'testunit-');
        $content1 = "Fichier crée pour un test unitaire";
        $content2 = "Fichier crée pour un test unitaire - fichier modifié";
        file_put_contents($tmpFile1, $content1);
        file_put_contents($tmpFile2, $content2);

        Filesystem::begin();
        Filesystem::rename($tmpFile1, $tmpFile2, true);

        $this->assertFalse(is_file($tmpFile1), 'Le fichier est renommé (transaction)');
        $this->assertEquals($content1, file_get_contents($tmpFile2), "Il a écraser l'autre fichier (transaction)");

        Filesystem::rollback();

        $this->assertEquals($content1, file_get_contents($tmpFile1), 'Le fichier est conservé après rollback');
        $this->assertEquals($content2, file_get_contents($tmpFile2), "Le fichier est restauré après rollback");

        Filesystem::begin();
        Filesystem::rename($tmpFile1, $tmpFile2, true);
        Filesystem::commit();

        $this->assertFalse(is_file($tmpFile1), 'Le fichier est renommé (commité)');
        $this->assertEquals($content1, file_get_contents($tmpFile2), "Il a écraser l'autre fichier (commit)");

        $this->safeUnlink($tmpFile1);
        $this->safeUnlink($tmpFile2);
    }

    public function testNestedTransactions()
    {
        $tmpFile1 = tempnam(sys_get_temp_dir(), 'testunit-');
        $tmpFile2 = tempnam(sys_get_temp_dir(), 'testunit-');
        $tmpFile3 = tempnam(sys_get_temp_dir(), 'testunit-');
        $content1 = "Fichier crée pour un test unitaire";
        $content2 = "Fichier crée pour un test unitaire - fichier modifié";
        $content3 = "Fichier crée pour un test unitaire - fichier modifié - encore";
        file_put_contents($tmpFile1, $content1);
        file_put_contents($tmpFile2, $content2);
        file_put_contents($tmpFile3, $content3);

        Filesystem::begin();
        Filesystem::rename($tmpFile1, $tmpFile2, true);
        Filesystem::begin('nested');
        Filesystem::rename($tmpFile2, $tmpFile3, true);

        $this->assertEquals($content1, file_get_contents($tmpFile3), 'Le fichier 3 contient désormais le fichier 1');

        Filesystem::rollback('nested');
        Filesystem::rollback();

        $this->assertEquals($content1, file_get_contents($tmpFile1), 'Le fichier 1 est restauré après rollback');
        $this->assertEquals($content2, file_get_contents($tmpFile2), "Le fichier 2 est restauré après rollback");
        $this->assertEquals($content3, file_get_contents($tmpFile3), "Le fichier 3 est restauré après rollback");

        $this->safeUnlink($tmpFile1);
        $this->safeUnlink($tmpFile2);
        $this->safeUnlink($tmpFile3);
    }

    public function testDumpFile()
    {
        $basePath = sys_get_temp_dir() . DS . 'testunit-mkdir';
        $dirToMake = $basePath . DS . 'with' . DS . 'Some' . DS . 'deep';
        $destination = $dirToMake . DS . 'testunit-destination' . DS . uniqid('gen');
        $content = "Fichier crée pour un test unitaire";

        Filesystem::begin();
        Filesystem::dumpFile($destination, $content);

        $this->assertEquals($content, file_get_contents($destination), 'Le fichier est bien créé');

        Filesystem::rollback();

        $this->assertFalse(is_file($destination), 'Le fichier est supprimé (rollback)');
        $this->assertFalse(is_dir($dirToMake), 'Le dossier est supprimé (rollback)');

        Filesystem::begin();
        Filesystem::dumpFile($destination, $content);
        Filesystem::commit();

        $this->assertEquals($content, file_get_contents($destination), 'Le fichier est bien créé (commité)');

        exec('rm '.escapeshellarg($basePath).' -R');

        $tmpFile = tempnam(sys_get_temp_dir(), 'testunit-');
        $content1 = "Fichier crée pour un test unitaire";
        $content2 = "Fichier crée pour un test unitaire - fichier modifié";
        file_put_contents($tmpFile, $content1);

        Filesystem::begin();
        Filesystem::dumpFile($tmpFile, $content2);

        $this->assertEquals($content2, file_get_contents($tmpFile), 'Le contenu du fichier est bien remplacé');

        Filesystem::rollback();

        $this->assertEquals($content1, file_get_contents($tmpFile), 'Le contenu du fichier est bien revenu (rollback)');

        $this->safeUnlink($tmpFile);
    }

    public function testShred()
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'testunit-zàâçéèêëîïôûùüÿñæœ-');
        $destination = sys_get_temp_dir() . DS . 'testunit-destination' . DS . uniqid('gen');

        Filesystem::begin();
        Filesystem::useShred(true);
        Filesystem::rename($tmpFile, $destination);
        Filesystem::commit();

        $this->assertFalse(is_file($tmpFile), 'Le fichier est renommé (commité)');

        $this->safeUnlink($tmpFile);
        $this->safeUnlink($destination);

        $file = tempnam(sys_get_temp_dir(), 'testunit-');
        file_put_contents($file, str_repeat("hello world,\n", 1000));
        $initialSize = filesize($file);
        $handle = fopen($file, 'r');
        $begin = fread($handle, 100);
        fseek($handle, $initialSize - 100);
        $end = fread($handle, 100);

        Filesystem::shred($file, ['remove' => false, 'iterations' => 1]);
        $this->assertFileExists($file);

        $newSize = filesize($file);
        $handle = fopen($file, 'r');
        $newBegin = fread($handle, 100);
        fseek($handle, $initialSize - 100);
        $newEnd = fread($handle, 100);

        $this->assertEquals($initialSize, $newSize);
        $this->assertNotEquals($begin, $newBegin);
        $this->assertNotEquals($end, $newEnd);

        Filesystem::shred($file, ['zero' => true, 'remove' => false, 'iterations' => 1]);
        $handle = fopen($file, 'r');
        $zero = fread($handle, 100);
        fclose($handle);
        $this->assertEquals(str_repeat(chr(0), 100), $zero);

        Filesystem::shred($file, ['remove' => true, 'iterations' => 2]);
        $this->assertFileDoesNotExist($file);

        $file = sys_get_temp_dir() . DS . 'testunit-destination';
        file_put_contents($file . DS . uniqid('gen'), 'hello world');
        Filesystem::shred($file);
        $this->assertFileDoesNotExist($file);
    }

    public function testExtractArbo()
    {
        $files = [
            '/tmp/file1',
            '/tmp/dir1/file2',
            '/tmp/dir1/file3',
            '/tmp/dir2/dir3/file4',
        ];
        $expected = [
            'tmp' => [
                'file1' => '/tmp/file1',
                'dir1' => [
                    'file2' => '/tmp/dir1/file2',
                    'file3' => '/tmp/dir1/file3',
                ],
                'dir2' => [
                    'dir3' => [
                        'file4' => '/tmp/dir2/dir3/file4',
                    ]
                ]
            ]
        ];

        $this->assertEquals($expected, Filesystem::extractArbo($files), "Extracted");
    }

    public function testBaseDir()
    {
        $files = [
            '/tmp/file1',
            '/tmp/dir1/file2',
            '/tmp/dir1/file3',
        ];
        $this->assertEquals('/tmp', Filesystem::baseDir($files), "Base DIR = /tmp");
        unset($files[0]); // /tmp/file1
        $this->assertEquals('/tmp/dir1', Filesystem::baseDir($files), "Base DIR = /tmp");

        $files = [
            '/tmp/dir1/dir2/dir3/file1',
            '/tmp/dir1/dir2/dir3/dir4/dir5/file2',
        ];
        $this->assertEquals('/tmp/dir1/dir2/dir3', Filesystem::baseDir($files), "Base DIR = /tmp/dir1/dir2/dir3");

        $files = [
            '/tmp/dir1/dir2/dir3/file1',
            '/data/dir1/dir2/dir3/dir4/dir5/file2',
        ];
        $this->assertEquals('/', Filesystem::baseDir($files), "Base DIR = /");
    }

    public function testCreateDummyFile()
    {
        $tmpDir = sys_get_temp_dir() . DS . 'testunit-destination';
        $targetSize = 4294967296; // MAX pour du 32 bits ~4Go

        Filesystem::createDummyFile($tmpDir . DS . 'dummy', $targetSize);
        $this->assertEquals($targetSize, filesize($tmpDir . DS . 'dummy'), 'Filesize désiré');
        $this->safeUnlink($tmpDir . DS . 'dummy');
    }

    protected function safeUnlink($filename)
    {
        if (is_file($filename)) {
            unlink($filename);
        }
    }
}
