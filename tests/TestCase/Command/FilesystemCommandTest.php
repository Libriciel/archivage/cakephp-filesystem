<?php

namespace Libriciel\Filesystem\Test\TestCase\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOutput;
use Libriciel\Filesystem\Command\FilesystemCommand;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

class FilesystemCommandTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $garbages = glob(sys_get_temp_dir() . DS . 'testunit*');
        if (!empty($garbages)) {
            Filesystem::setNamespace('no-transaction');
            Filesystem::remove($garbages);
        }
        /** @noinspection PhpFullyQualifiedNameUsageInspection */
        if (class_exists('Cake\Cache\Cache') && empty(\Cake\Cache\Cache::getConfig('_cake_core_'))) {
            /** @noinspection PhpFullyQualifiedNameUsageInspection */
            \Cake\Cache\Cache::setConfig(
                [
                    '_cake_core_' => [
                        'className' => 'File',
                        'prefix' => 'myapp_cake_core_',
                        'path' => TMP . 'testunit-cache/',
                        'serialize' => true,
                        'duration' => '+1 second',
                        'url' => env('CACHE_CAKECORE_URL'),
                    ],
                ]
            );
        }
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    public function testMain()
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'testunit-');
        $content = "Fichier crée pour un test unitaire";
        file_put_contents($tmpFile, $content);
        $destination = sys_get_temp_dir() . DS . 'testunit-destination' . DS . uniqid('gen');

        /**
         * Copy 1->2 / Delete 1 / Rename 2->1
         */
        Filesystem::begin();
        Filesystem::copy($tmpFile, $destination);
        $this->assertFileExists($destination);
        Filesystem::remove($tmpFile);
        $this->assertFileDoesNotExist($tmpFile);
        Filesystem::rename($destination, $tmpFile);
        $this->assertFileDoesNotExist($destination);
        $this->assertFileExists($tmpFile);

        // On change le pid du dossier de backup pour forcer le rollback par shell
        $pid = $this->changePidDir();

        /** @var FilesystemCommand|MockObject $shell */
        $output = tempnam(sys_get_temp_dir(), 'testunit-');
        $co = new ConsoleOutput($output);
        $io = new ConsoleIo($co, $co);
        $shell = new FilesystemCommand();
        $shell->execute(new Arguments([], [], []), $io);
        Filesystem::getInstance()->toCallForRollback = [];
        Filesystem::getInstance()->toCallForCommit = [];
        $this->assertStringContainsString('Rollback process '.$pid, file_get_contents($output));

        $this->assertFileExists($tmpFile);
        $this->assertFileDoesNotExist($destination);

        /**
         * Delete 1 / Create 1 / Change 1->2
         */
        $md5_1 = md5_file($tmpFile);
        Filesystem::remove($tmpFile);
        $this->assertFileDoesNotExist($tmpFile);
        Filesystem::createDummyFile($tmpFile, 1024);
        $this->assertFileExists($tmpFile);
        $md5_2 = md5_file($tmpFile);
        $this->assertNotEquals($md5_1, $md5_2);
        Filesystem::dumpFile($tmpFile, 'fichier modifié');
        $md5_3 = md5_file($tmpFile);
        $this->assertNotEquals($md5_2, $md5_3);

        $pid = $this->changePidDir();
        $shell->execute(new Arguments([], [], []), $io);
        Filesystem::getInstance()->toCallForRollback = [];
        Filesystem::getInstance()->toCallForCommit = [];
        $this->assertStringContainsString('Rollback process '.$pid, file_get_contents($output));
        $this->assertFileExists($tmpFile);
        $this->assertEquals($md5_1, md5_file($tmpFile));
        unlink($tmpFile);
        unlink($output);
    }

    private function changePidDir()
    {
        $pid = getmypid();
        $processPath = Filesystem::TRANSACTION_DIR . DS . $pid;
        while (file_exists('/proc/'.$pid) || is_dir(Filesystem::TRANSACTION_DIR . DS . $pid)) {
            $pid++;
        }
        $filesystem = new \Symfony\Component\Filesystem\Filesystem;
        $filesystem->rename($processPath, Filesystem::TRANSACTION_DIR . DS . $pid);
        return $pid;
    }

    public function testRemoveEmptySubFolders()
    {
        mkdir(TMP_TESTDIR.DS.'foo', 0777, true);
        mkdir(TMP_TESTDIR.DS.'foo'.DS.'bar');
        mkdir(TMP_TESTDIR.DS.'foo'.DS.'bar'.DS.'baz');
        mkdir(TMP_TESTDIR.DS.'foo'.DS.'biz');
        touch(TMP_TESTDIR.DS.'foo'.DS.'biz'.DS.'test.txt');

        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator(
                TMP_TESTDIR.DS.'foo'
            )
        );
        $paths = [];
        $len = mb_strlen(TMP_TESTDIR.DS);
        /** @var SplFileInfo $spl */
        foreach ($iterator as $spl) {
            if ($spl->getFilename() === '.') {
                $paths[] = mb_substr($spl->getPath(), $len);
            } elseif ($spl->getFilename() !== '..') {
                $paths[] = mb_substr($spl->getPathname(), $len);
            }
        }
        sort($paths);
        $this->assertEquals(
            [
                0 => 'foo',
                1 => 'foo/bar',
                2 => 'foo/bar/baz',
                3 => 'foo/biz',
                4 => 'foo/biz/test.txt'
            ],
            $paths
        );

        Filesystem::removeEmptySubFolders(TMP_TESTDIR.DS.'foo');

        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator(
                TMP_TESTDIR.DS.'foo'
            )
        );
        $paths = [];
        /** @var SplFileInfo $spl */
        foreach ($iterator as $spl) {
            if ($spl->getFilename() === '.') {
                $paths[] = mb_substr($spl->getPath(), $len);
            } elseif ($spl->getFilename() !== '..') {
                $paths[] = mb_substr($spl->getPathname(), $len);
            }
        }
        sort($paths);
        $this->assertEquals(
            [
                0 => 'foo',
                1 => 'foo/biz',
                2 => 'foo/biz/test.txt'
            ],
            $paths
        );
    }

    public function testRemoveFolderWithHiddenFiles()
    {
        foreach ([true, false] as $useShred) {
            mkdir(TMP_TESTDIR.DS.'foo', 0777, true);
            mkdir(TMP_TESTDIR.DS.'foo'.DS.'bar');
            file_put_contents(TMP_TESTDIR.DS.'foo'.DS.'bar'.DS.'.hidden', 'test');
            Filesystem::useShred($useShred);
            Filesystem::remove(TMP_TESTDIR.DS.'foo');
            $this->assertDirectoryDoesNotExist(TMP_TESTDIR.DS.'foo');
        }
    }
}
