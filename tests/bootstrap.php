<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Libriciel\Filesystem\Utility\Filesystem;

$root = dirname(__DIR__);
require $root . '/vendor/autoload.php';

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('TMP')) {
    define('TMP', sys_get_temp_dir() . DS);
}
if (getenv('TEST_TOKEN')) {
    putenv('NO_COLOR=true'); // désactive les couleurs de debug pour un bon affichage avec paratest
}
$tokenSuffix = getenv('TEST_TOKEN') ? '_'.getenv('TEST_TOKEN') : '';
if (!defined('TMP_TESTDIR')) {
    define('TMP_TESTDIR', sys_get_temp_dir().DS.'testunit'.$tokenSuffix);
}
if (!defined('CACHE')) {
    define('CACHE', sys_get_temp_dir() . '/cache/');
}
Configure::write(
    [
        'App' => [
            'namespace' => 'Libriciel\Filesystem',
            'encoding' => env('APP_ENCODING', 'UTF-8'),
            'defaultLocale' => env('APP_DEFAULT_LOCALE', 'fr_FR'),
            'base' => false,
            'dir' => 'src',
            'fullBaseUrl' => false,
            'paths' => [
                dirname(__DIR__) . '/src/Locale',
            ],
        ],
        'Cache' => [
            'default' => [
                'className' => 'File',
                'path' => CACHE,
                'url' => env('CACHE_DEFAULT_URL', null),
            ],
            '_cake_core_' => [
                'className' => 'File',
                'prefix' => 'myapp_cake_core_',
                'path' => CACHE . 'persistent/',
                'serialize' => true,
                'duration' => '+1 years',
                'url' => env('CACHE_CAKECORE_URL', null),
            ],
            '_cake_model_' => [
                'className' => 'File',
                'prefix' => 'myapp_cake_model_',
                'path' => CACHE . 'models/',
                'serialize' => true,
                'duration' => '+1 years',
                'url' => env('CACHE_CAKEMODEL_URL', null),
            ],
        ],
        'FilesystemUtility' => [
            'useShred' => false,
        ],
        'Session' => [
            'defaults' => 'php',
        ],
        'debug' => true,
    ]
);

Filesystem::$useShred = false;

Cache::setConfig(Configure::consume('Cache'));
